from flask import Flask, request, jsonify, render_template, url_for, redirect
from flask_cors import CORS, cross_origin
from flask_sqlalchemy import SQLAlchemy 
from flask_marshmallow import Marshmallow  
from flask_socketio import SocketIO, send
from datetime import datetime, time
from passlib.context import CryptContext
import os
import json

app = Flask(__name__, template_folder='template')
CORS(app)
app.config['SECRET_KEY'] = 'BianchiOltre'
socketio = SocketIO(app)

#setting up the base directory
basedir = os.path.abspath(os.path.dirname(__file__))

# Database config
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'student.sqlite')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_ECHO'] = True

db = SQLAlchemy(app)

pwd_context = CryptContext(schemes=["pbkdf2_sha256"], default="pbkdf2_sha256", pbkdf2_sha256__default_rounds=30000)


ma = Marshmallow(app)

# Student Class == Database Model
class Student(db.Model):
  id = db.Column(db.Integer, primary_key=True)
  studentid = db.Column(db.String(100)) 
  name = db.Column(db.String(100))
  subject = db.Column(db.String(50))
  date = db.Column(db.DateTime, default=datetime.utcnow)

  def __init__(self, studentid ,name, subject , date):
  	self.studentid = studentid
  	self.name = name
  	self.subject = subject
  	self.date = date

# Student Schema - defining student Schema
class StudentSchema(ma.Schema):
  class Meta:
    fields = ('id', 'studentid' ,'name','subject', 'date')


Student_schema = StudentSchema()
Students_schema = StudentSchema(many=True)

class User(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	userid = db.Column(db.String(100)) 
	password = db.Column(db.String(1000))
	def __init__(self, userid ,password):
		self.userid = userid
		self.password = password

class UserSchema(ma.Schema):
	class Meta:
		fields = ('id', 'userid','password')

User_schema = UserSchema()
Users_schema = UserSchema(many=True)

# Initialize schema
db.create_all()


def check_encrypted_password(password, hashed):
	return pwd_context.verify(password, hashed)

def encrypt_password(password):
	return pwd_context.encrypt(password)

# Create a student profile
@app.route('/add', methods=['POST'])
def add_student():
  studentid = request.json['studentid']
  name = request.json['name']
  subject = request.json['subject']
  date = datetime.now()

  new_student = Student( studentid, name, subject, date)

  db.session.add(new_student)
  db.session.commit()

  return Student_schema.jsonify(new_student)


@app.route('/alpeduhuez', methods=['GET'])
def homepage():
	return render_template('./login.html')

@app.route('/login', methods=['POST'])
def login():
	data = request.get_json(force=True)
	#print(type(data))
	#print(data.decode('utf-8'))
	#data = json.loads(dat.decode("utf-8"))
	valid = False
	username = data['username'].strip()
	password = data['password']
	print(username)
	print(password)
	user_profile = User.query.filter_by(userid = username).first()
	result = User_schema.dump(user_profile)
	#print(result)
	if check_encrypted_password(password, result['password']) == True:
		valid = True
		print(valid)
	if valid == True:
		print({'valid':'Authenticated'})
		return jsonify({'valid':'Authenticated'}) 
	else:
		print({'valid':'Un - Authenticated'})
		return jsonify({'valid':'Access Denied'})





# Get All students details
@app.route('/student', methods=['GET'])
def get_students():
  all_students = Student.query.all()
  result = Students_schema.dump(all_students)
  output = {'students':result.data}
  return jsonify(output)

# Get All subject details
@app.route('/subject', methods=['GET'])
def get_subjects():	
  subject = request.get_json(force=True)
  subject_students = Student.query.filter(Student.subject == subject['subject'])
  result = Students_schema.dump(subject_students)
  return jsonify(result.data)

@app.route('/attendance',methods=['GET'])
def home():
	return render_template('./index.html')


@app.route('/admin', methods=['POST','GET'])
def admin():
    return render_template('./admin.html')

@app.route('/start', methods=['GET'])
def start():
  data = False
  day_of_week = datetime.now().weekday()

  if day_of_week == 1 or day_of_week == 2 or day_of_week == 5:
    current_date = datetime.now()
    current_time = time(current_date.hour, current_date.minute, current_date.second)
    if time(8,15,00) < current_time < time(10,5,00):
      data = True
    elif time(12,15,00) < current_time < time(14,5,00):
      data = True
    elif time(22,10,00) < current_time < time(23,5,00):
      data = True

  return jsonify({'value':data})  
      


def insertUser(userid, password):
	encrypt_pass = encrypt_password(password)
	new_user = User(userid, encrypt_pass)
	db.session.add(new_user)
	db.session.commit()


@socketio.on('message')
def handleMessage(msg):
	print('Message: ' + str(msg))
	studentid = msg['student_id']
	name = msg['student_name']
	subject = msg['subject']
  
	date = datetime.now()
	new_student = Student( studentid, name, subject, date)
	db.session.add(new_student)
	db.session.commit()
	msg['id'] = new_student.id
	print(new_student.id)
	send(msg, broadcast=True)

if __name__ == '__main__':
	socketio.run(app, host='0.0.0.0', debug=True)
